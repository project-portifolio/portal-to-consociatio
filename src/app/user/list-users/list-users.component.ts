import { Component, OnInit } from '@angular/core';
import { UserService } from './../user.service';
import { ExcelService } from '../../shared/services/excel/excel.service';
import * as XLSX from 'ts-xlsx';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { AlertService } from '../../shared/componentes/alert/alert.service';
import { AuthGuard } from '../../shared';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.scss']
})
export class ListUsersComponent implements OnInit {

  users: any = [{ id: '', nome: '', email: '', nivel: '', status: '', fone: '' }];
  listUsers: any = [];
  list: any = [];
  arrayBuffer: any;
  file: File;
  textLabel: string = 'Expandir';
  hiddenFiltro = false;
  filter: any = {};
  p: any;
  user: any = {};
  hidden: boolean = false;
  closeResult: string;

  constructor(private userService: UserService,
    private excelService: ExcelService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private alertService: AlertService,
    private authGuard: AuthGuard,
    private modalService: NgbModal) {

      let verify = authGuard.canActivate();
      if(verify == true) {
        this.user = authGuard.getUserLogado();
        if(this.user.nivel == 5) {
          this.hidden = true;
        } else {
          this.hidden = false;
        }
      }

    }

  ngOnInit() {
    this.loadPage();
  }

  // LOADING SHOW
  loadingShow() {
    this.spinner.show();
  }

  // LOADING HIDE
  loadingHide() {
    this.spinner.hide();
  }

  loadPage() {
    if (!this.filter.page) {
      this.filter.page = 1;
    }
    if (!this.filter.pageSize) {
      this.filter.pageSize = 10;
    }
    this.getAllUsers();
  }

  // cadastrar novo usuário
  openRegister() {
    this.router.navigate(['users/register']);
  }

  // editar usuário
  onEdit(u) {
    this.router.navigate(['users/details/' + u.id]);
  }

  // listando todos os usuários
  getAllUsers() {
    this.loadingShow();
    this.userService.getAllUsers().subscribe( response => {
      this.users = response;
      if(this.user.nivel != 5) {
        this.users.forEach(element => {
          if(element.nivel == 5) {
            this.users.splice(element, 1);
          }
        });
      }
      this.loadingHide();
    }, error => {
      console.log(error);
      this.loadingHide();
      this.alertService.sendMessage({ title: 'Erro!', msg: 'Não foi possível carregar a lista de usuários!' });
    });
  }

  // exportando arquivo excel com lista de usuários
  exportAsXLSX():void {
    this.excelService.exportAsExcelFile(this.users, 'lista_de_usuários');
  }

  // escolhendo arquivo excel
  incomingfile(event) {
    this.file= event.target.files[0]; 
  }

  // fazendo upload de arquivo excel
  Upload() {
    let fileReader = new FileReader();
      fileReader.onload = (e) => {
        this.arrayBuffer = fileReader.result;
        var data = new Uint8Array(this.arrayBuffer);
        var arr = new Array();
        for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
        var bstr = arr.join("");
        var workbook = XLSX.read(bstr, {type:"binary"});
        var first_sheet_name = workbook.SheetNames[0];
        var worksheet = workbook.Sheets[first_sheet_name];
        console.log(XLSX.utils.sheet_to_json(worksheet,{raw:true}));
        this.listUsers = XLSX.utils.sheet_to_json(worksheet,{raw:true});
        for(let i = 0; i < this.listUsers.length; i++) {
          this.list.push(this.listUsers[i]);
        }
        this.saveListUsers(this.list);
      }
      fileReader.readAsArrayBuffer(this.file);
  }

  // salvando lista de usuários
  saveListUsers(listUsers) {
    this.loadingShow();
    this.userService.saveListUsers(listUsers).subscribe( response => {
      this.users = response;
      this.alertService.success({ title: 'Sucesso!', msg: 'Planilha importada com sucesso!' });
      this.ngOnInit();
      this.loadingHide();
    }, error => {
      console.log(error);
      this.alertService.sendMessage({ title: 'Erro!', msg: 'Não foi possível importar a planilha de usuários!' });
      this.loadingHide();
    });
  }
  
  // atualizando status
  updateStatus(user) {
    this.userService.updateStatus(user).subscribe( response => {
      console.log(response);
      console.log('Atualizando o usuário!');
      this.alertService.success({ title: 'Sucesso!', msg: 'Status alterado com sucesso!' });
      this.getAllUsers();
    }, error => {
      console.log(error);
      this.alertService.sendMessage({ title: 'Erro!', msg: 'Não foi possível alterar o status!' });
    });
  }

  // remover usuário
  removeUser(user) {
    this.loadingShow();
    this.userService.removeUser(user).subscribe( response => {
      console.log(response);
      this.alertService.success({ title: 'Sucesso!', msg: 'Usuário excluído com sucesso!' });
      this.getAllUsers();
      this.loadingHide();
    }, error => {
      console.log(error);
      this.loadingHide();
      this.alertService.sendMessage({ title: 'Erro!', msg: 'Não foi possível excluír usuário!' });
    });
  }

  // ocultar/expandir filtros
  toggleFiltro() {
    if(this.hiddenFiltro) {
      this.hiddenFiltro = false;
      this.textLabel = 'Expandir';
    } else {
      this.hiddenFiltro = true;
      this.textLabel = 'Ocultar';
    }
  }

  // ABRINDO MODAL DE CONFIRMAÇÃO DE REGISTRO
  openModalDelete(content, user) {
    this.modalService.open(content, { centered: true }).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
        if(result === 'Confirm click') {
          console.log('Deletando registro!');
          this.removeUser(user);
        } else if(result === 'Close click') {
          console.log('Cancelar operação!');
        }
    }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  // CAPTURANDO OPÇÕES DE FECHAMENTO DE MODAL DE EXCLUSÃO DE REGISTRO
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
        return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return 'by clicking on a backdrop';
    } else {
        return  `with: ${reason}`;
    }
  }

}
