import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListUsersComponent } from './list-users/list-users.component';
import { ProfileUserComponent } from './profile-user/profile-user.component';
import { UserRoutingModule } from './user-routing.modules';
import { UserService } from './user.service';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserComponent } from './profile-user/user/user.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PermissaoComponent } from './profile-user/permissao/permissao.component';
import { NgxPaginationModule } from 'ngx-pagination';
import {NgxMaskModule} from 'ngx-mask';

@NgModule({
  imports: [
    CommonModule,
    UserRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
    NgxMaskModule.forRoot()
  ],
  declarations: [
    ListUsersComponent, 
    ProfileUserComponent, 
    UserComponent, 
    PermissaoComponent
  ],
  exports: [
    ListUsersComponent,
    ProfileUserComponent,
    UserComponent,
    NgbModule
  ],
  providers: [
    UserService
  ]
})
export class UserModule { }
