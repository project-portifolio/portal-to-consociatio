import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../user.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-permissao',
  templateUrl: './permissao.component.html',
  styleUrls: ['./permissao.component.scss']
})
export class PermissaoComponent implements OnInit {

  id: number;
  permissoes: any = [{id: '', titulo:'', autorizacao: '', descricao: ''}];
  userDTo:any = {id: '', idPermissao: []};
  listPermissoes: Array<any> = [];
  labelSuccess: string = '';
  legBtnSave: string = '';

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.getAllPermissoes();
     // RECUPERANDO O ID PASSADO COMO PARÂMETRO NA URL
     this.activatedRoute.params.subscribe( params => {
      if (!(params && params.id && params.id != 'register')) {
        this.legBtnSave = 'Cadastrar Permissão';
        this.labelSuccess = 'Cadastrar';
        return [];
      } else {
          console.log(params.id);
          this.id = +params['id'];
          this.legBtnSave = 'Atualizar Permissão';
          this.labelSuccess = 'Salvar';
          this.loadingShow();
        }
     });
  }

  // LOADING SHOW
  loadingShow() {
    this.spinner.show();
  }

  // LOADING HIDE
  loadingHide() {
    this.spinner.hide();
  }

  // BUSCANDO TODAS AS PERMISSÕES
  getAllPermissoes() {
    this.loadingShow();
    this.userService.getAllPermissoes().subscribe( response => {
      this.permissoes = response;
    
      this.onSearchPermissao();

      this.loadingHide();
    }, error => {
      console.log(error);
      this.loadingHide();
    });
  }

  // SELECIONANDO AS PERMISSÕES
  onChange(id:number, isChecked: boolean) {
    console.log('ID: ' + id + ' - Boolean: ' + isChecked);
    if(isChecked) {
      this.listPermissoes.push(id);
    } else {
      let index = this.listPermissoes.indexOf(id);
      this.listPermissoes.splice(index,1);
    }
  }

  onSearchPermissao() {
    this.permissoes.forEach(element => {
      console.log(element);
    });
  }

  ngOnSubmit() {
    this.loadingShow();
    if(this.listPermissoes) {
      console.log('Id do usuário: ' + this.id + ' - Lista de permissões: ' + this.listPermissoes);
      this.userDTo.id = this.id;
      this.userDTo.idPermissao = this.listPermissoes;
      this.userService.linkPermissaoUser(this.userDTo).subscribe( response => {
        this.userDTo = response;
        this.router.navigate(['/users']);
        this.loadingHide();
      }, error => {
        console.log(error);
        this.loadingHide();
      });
    } else {
      console.log('Nenhuma permissão selecionada!');
      this.loadingHide();
    }
  }

}
