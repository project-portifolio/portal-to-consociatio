import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators, ReactiveFormsModule } from '@angular/forms';
import { UserService } from './../user.service';

@Component({
  selector: 'app-profile-user',
  templateUrl: './profile-user.component.html',
  styleUrls: ['./profile-user.component.scss']
})
export class ProfileUserComponent implements OnInit {

  id: number;
  title: string;

  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    // RECUPERANDO O ID PASSADO COMO PARÂMETRO NA URL
    this.activatedRoute.params.subscribe( params => {
      if (!(params && params.id && params.id != 'register')) {
        this.title = 'Novo usuário';
        return [];
      } else {
          console.log(params.id);
          this.id = +params['id'];
          this.title = 'Editar usuário';
        }
     });
  }

}
