import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ReactiveFormsModule } from '@angular/forms';
import { UserService } from '../../user.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { AlertService } from '../../../shared/componentes/alert/alert.service';
import { AuthGuard } from '../../../shared';
import { NgxMaskModule } from 'ngx-mask';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  id: number;
  dForm: FormGroup;
  user: any = { id: '', nome: '', email: '', nivel: '', status: '', fone: '' };
  userDB;
  selectedNivel;
  nivel: number;
  status: string;
  legBtnSave: string = '';
  title: string;
  labelSuccess: string;
  hidden: boolean = false;

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private userService :UserService,
    private spinner: NgxSpinnerService,
    private alertService: AlertService,
    private authGuard: AuthGuard) { 

      let verify = authGuard.canActivate();
      if(verify == true) {
        this.user = authGuard.getUserLogado();
        console.log(this.user);
        if(this.user.nivel == 5) {
          this.hidden = true;
        } else {
          this.hidden = false;
        }
      }

    }

  ngOnInit() {
    this.criaForm();
     // RECUPERANDO O ID PASSADO COMO PARÂMETRO NA URL
     this.activatedRoute.params.subscribe( params => {
      if (!(params && params.id && params.id != 'register')) {
        this.legBtnSave = 'Cadastrar usuário';
        this.labelSuccess = 'Cadastrar';
        return [];
      } else {
          console.log(params.id);
          this.id = +params['id'];
          this.findUserById(this.id);
          this.legBtnSave = 'Atualizar usuário';
          this.labelSuccess = 'Salvar';
        }
     });
  }

  // LOADING SHOW
  loadingShow() {
    this.spinner.show();
  }

  // LOADING HIDE
  loadingHide() {
    this.spinner.hide();
  }

  ngOnSubmit() {
    this.user.id = this.dForm.value.id;
    this.user.nome = this.dForm.value.nome;
    this.user.email = this.dForm.value.email;
    this.user.fone = this.dForm.value.fone;
    this.user.nivel = this.dForm.value.nivel;
    this.user.status = this.dForm.value.status;
    this.updateSaveUser(this.user);
    this.status = this.returnStatus(this.user.status);
  }

  // CRIANDO FORMULÁRIO
  criaForm() {
    this.dForm = this.fb.group({
      id: [null],
      nome: [null, Validators.required],
      email: [null, Validators.required],
      fone: [null, Validators.required],
      nivel: [null, Validators.required],
      status: [null, Validators.required]
    });
  }

  // RETORNANDO O USUÁRIO PELO ID
  findUserById(id) {
    this.loadingShow();
    return this.userService.findUserById(id).subscribe( response => {
      this.userDB = response; 
      this.dForm.patchValue({
        id: this.userDB.id,
        nome: this.userDB.nome,
        email: this.userDB.email,
        fone: this.userDB.fone,
        nivel: this.userDB.nivel,
        status: this.userDB.status
      });

      this.nivel = this.userDB.nivel;
      this.returnStatus(this.userDB.status);
      this.loadingHide();
    }, error => {
      console.log(error);
      this.loadingHide();
      this.alertService.sendMessage({ title: 'Erro!', msg: 'Não foi possível carregar o usuário!' });
    });
  }

  // RETORNANDO O STATUS
  returnStatus(status) {
    switch(status) {
      case true:
        this.status = 'Ativo';
        break;
      case false:
        this.status = 'Inativo';
        break;
    }
    return status;
  }

  // UPDATE SAVE USUÁRIO
  updateSaveUser(user) {
    this.loadingShow();
    this.userService.updateSaveUser(user).subscribe( response => {
      this.user = response;
      this.alertService.success({ title: 'Sucesso!', msg: 'Usuário salvo com sucesso!' });
      this.findUserById(this.user.id);
      this.loadingHide();
    }, error => {
      console.log(error);
      this.alertService.sendMessage({ title: 'Erro!', msg: 'Não foi possível salvar o associado!' });
      this.loadingHide();
    });
  }
}
