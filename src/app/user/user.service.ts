import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './../shared/services/api.service';

@Injectable()
export class UserService {

  constructor(private http: HttpClient, 
    private api: ApiService) { }

    // RETURN ALL USERS
    public getAllUsers() {
      return this.http.get(this.api.getBaseUrl() + 'usuario/getAllUsers', this.api.getOptions());
    }

    // SAVE LIST USER
    public saveListUsers(listUsers) {
      return this.http.put(this.api.getBaseUrl() + 'usuario/saveListUsers', listUsers, this.api.getOptions());
    }

    // UPDATE STATUS USER
    public updateStatus(user) {
      return this.http.get(this.api.getBaseUrl() + 'usuario/updateStatus/'+ user.status + '/' + user.id, this.api.getOptions());
    }

    // REMOVER USUÁRIO
    public removeUser(user) {
      return this.http.delete(this.api.getBaseUrl() + 'usuario/removeUser/' + user.id, this.api.getOptions());
    }

    // FIND USER BU ID
    public findUserById(id) {
      return this.http.get(this.api.getBaseUrl() + 'usuario/findUserById/' + id, this.api.getOptions());
    }

    // UPDATE SAVE USER
    public updateSaveUser(user) {
      return this.http.post(this.api.getBaseUrl() + 'usuario/updateUser', user, this.api.getOptions());
    }

    // BUSCANDO TODAS AS PERMISSÕES
    public getAllPermissoes() {
      return this.http.get(this.api.getBaseUrl() + 'permissao/getAllPermissoes', this.api.getOptions());
    }

    // VINCULANDO PERMISSÃO AO USUÁRIO
    public linkPermissaoUser(userDto) {
      return this.http.put(this.api.getBaseUrl() + 'usuario/linkPermissaoUser', userDto, this.api.getOptions());
    }

}
