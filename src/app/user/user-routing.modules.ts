import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListUsersComponent } from './list-users/list-users.component';
import { ProfileUserComponent } from './profile-user/profile-user.component';

const routes: Routes = [
    { path: '', component: ListUsersComponent },
    { path: '', component: ProfileUserComponent },
    { path: ':id', component: ProfileUserComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UserRoutingModule {}
