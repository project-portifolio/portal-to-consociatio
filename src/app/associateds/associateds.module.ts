import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListAssociatedsComponent } from './list-associateds/list-associateds.component';
import { ProfileAssociatedComponent } from './profile-associated/profile-associated.component';
import { AssociatedsRoutingModule } from './associateds-routing.modules';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AssociatedsService } from './associateds.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
import { ModalEmailComponent } from '../shared/componentes/modal-email/modal-email.component';
import { SendEmailService } from '../shared/componentes/modal-email/send-email.service';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  imports: [
    CommonModule,
    AssociatedsRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
    NgxMaskModule.forRoot()
  ],
  declarations: [
    ListAssociatedsComponent, 
    ProfileAssociatedComponent,
    ModalEmailComponent
  ],
  exports: [
    ListAssociatedsComponent,
    ProfileAssociatedComponent
  ],
  providers: [
    AssociatedsService,
    SendEmailService
  ]
})
export class AssociatedsModule { }
