import { ApiService } from './../shared/services/api.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class AssociatedsService {

  constructor(private http: HttpClient,
  private api: ApiService) { }

  // UPDATE SAVE ASSOCIADO
  public updateSaveAssociado(associado) {
    return this.http.post(this.api.getBaseUrl() + 'associados/updateSaveAssociado', associado, this.api.getOptions());
  }

  // FIND BY ID ASSOCIADO
  public findAssociadoById(id) {
    return this.http.get(this.api.getBaseUrl() + 'associados/findAssociadoById/' + id, this.api.getOptions());
  }

  // GET ALL ASSOCIADOS
  public getAll() {
    return this.http.get(this.api.getBaseUrl() + 'associados/getAll', this.api.getOptions());
  }

  // SAVE LIST ASSOCIADOS
  public saveListAssociados(listAssociados) {
    return this.http.put(this.api.getBaseUrl() + 'associados/saveListAssociados', listAssociados, this.api.getOptions());
  }

  // BUSCAR ASSOCIADO POR NOME
  public getAssociadoByLikeNome(nome) {
    return this.http.get(this.api.getBaseUrl() + 'associados/getAssociadoByLikeNome/' + nome, this.api.getOptions());
  }

  // BUSCAR ASSOCIADO POR LOTAÇÃO 
  public getAssociadoByLotacao(lotacao) {
    return this.http.get(this.api.getBaseUrl() + 'associados/getAssociadoByLotacao/' + lotacao, this.api.getOptions());
  }

  // BUSCAR ASSOCIADO PELA MATRÍCULA
  public filterByMatricula(matricula) {
    return this.http.get(this.api.getBaseUrl() + 'associados/filterByMatricula/' + matricula, this.api.getOptions());
  }

  // UPDATE STATUS ASSOCIADO
  public updateStatus(associado) {
    return this.http.get(this.api.getBaseUrl() + 'associados/updateStatus/'+ associado.status + '/' + associado.id, this.api.getOptions());
  }

  // REMOVER ASSOCIADO
  public removeAssociado(associado) {
    return this.http.delete(this.api.getBaseUrl() + 'associados/removeAssociado/' + associado.id, this.api.getOptions());
  }

  // RETORNANDO A ÚLTIMA MATRÍCULA
  public findLastByMatricula() {
    return this.http.get(this.api.getBaseUrl() + 'associados/findLastByMatricula', this.api.getOptions());
  }

  // BUSCANDO UMA LISTA DE ANIVERSARIANTES
  public getListAniversariantes(dataInicio, dataFim) {
    return this.http.get(this.api.getBaseUrl() + 'associados/getListAniversariantes/' + dataInicio + '/' + dataFim, this.api.getOptions());
  }

}
