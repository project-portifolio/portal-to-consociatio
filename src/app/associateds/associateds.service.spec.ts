import { TestBed, inject } from '@angular/core/testing';

import { AssociatedsService } from './associateds.service';

describe('AssociatedsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AssociatedsService]
    });
  });

  it('should be created', inject([AssociatedsService], (service: AssociatedsService) => {
    expect(service).toBeTruthy();
  }));
});
