import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListAssociatedsComponent } from './list-associateds/list-associateds.component';
import { ProfileAssociatedComponent } from './profile-associated/profile-associated.component';

const routes: Routes = [
    { path: '', component: ListAssociatedsComponent },
    { path: '', component: ProfileAssociatedComponent },
    { path: ':id', component: ProfileAssociatedComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AssociatedsRoutingModule {}
