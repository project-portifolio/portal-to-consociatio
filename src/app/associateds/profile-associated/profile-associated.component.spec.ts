import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileAssociatedComponent } from './profile-associated.component';

describe('ProfileAssociatedComponent', () => {
  let component: ProfileAssociatedComponent;
  let fixture: ComponentFixture<ProfileAssociatedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileAssociatedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileAssociatedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
