import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { AssociatedsService } from '../associateds.service';
import { AlertService } from '../../shared/componentes/alert/alert.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgxMaskModule } from 'ngx-mask';
import { AuthGuard } from '../../shared';

@Component({
  selector: 'app-profile-associated',
  templateUrl: './profile-associated.component.html',
  styleUrls: ['./profile-associated.component.scss']
})
export class ProfileAssociatedComponent implements OnInit {

  id: number;
  title: string;
  dForm: FormGroup;
  regioes: any;
  estados: any;
  legBtnSave: string = '';
  labelSuccess: string = '';
  associadoDB: any = {};
  user: any;
  hidden: boolean = false;
  lastMatricula: number;
  
  associado: any = { 
    id: '',
    nome: '',
    email: '',
    dataNascimento: '',
    fone: '',
    matricula: '',
    cpf: '',
    lotacao: '',
    regiao: '',
    tipo: '',
    status: ''
  };

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private associatedsService: AssociatedsService,
    private spinner: NgxSpinnerService,
    private alertService: AlertService,
    private authGuard: AuthGuard) {
      let verify = authGuard.canActivate();
      if(verify == true) {
        this.user = authGuard.getUserLogado();
        console.log(this.user);
        if(this.user.nivel == 5) {
          this.hidden = true;
        } else {
          this.hidden = false;
        }
      }
    }

  ngOnInit() {
    this.lastMatricula = 1;
    // RECUPERANDO O ID PASSADO COMO PARÂMETRO NA URL
    this.activatedRoute.params.subscribe( params => {
      if(!(params && params.id && params.id != 'register')) {
        this.criaForm();
        this.title = 'Novo associado';
        this.legBtnSave = 'Cadastrar Associado';
        this.labelSuccess = 'Cadastrar';
        this.associatedsService.findLastByMatricula().subscribe((response: any) => {
          this.lastMatricula = response + this.lastMatricula;
          this.criaForm();
        }, error => {
          console.log(error);
        });
        return [];
      } else {
        this.criaForm();
        console.log(params.id);
        this.id = +params['id'];
        this.title = 'Editar associado';
        this.legBtnSave = 'Atualizar Associado';
        this.labelSuccess = 'Salvar';
        this.findAssociadoById(this.id);
      }
    });
  }

  // LOADING SHOW
  loadingShow() {
    this.spinner.show();
  }

  // LOADING HIDE
  loadingHide() {
    this.spinner.hide();
  }

  // CRIANDO FORMULÁRIO
  criaForm() {
    this.dForm = this.fb.group({
      id: [null],
      nome: [null, Validators.required],
      email: [null],
      dataNascimento: [null],
      fone: [null, Validators.required],
      matricula: this.lastMatricula,
      cpf: [null, Validators.required],
      lotacao: [null],
      // regiao: [null],
      // tipo: [null],
      status: [null, Validators.required],
      // cep: [null],
      // estado: [null],
      // cidade: [null],
      // bairro: [null],
      // rua: [null],
      // numero: [null]
    });
  }

  // SALVANDO ASSOCIADO
  save(type) {

    console.log('Here!!');
    if(!this.dForm.value.nome || !this.dForm.value.email || !this.dForm.value.dataNascimento || !this.dForm.value.fone || 
      !this.dForm.value.cpf || !this.dForm.value.status || !this.dForm.value.matricula) {
        return;
    }

    this.associado.id = this.dForm.value.id;
    this.associado.nome = this.dForm.value.nome;
    this.associado.email = this.dForm.value.email;
    this.associado.dataNascimento = this.formatData(this.dForm.value.dataNascimento);
    this.associado.fone = this.dForm.value.fone;
    this.associado.matricula = this.dForm.value.matricula;
    this.associado.cpf = this.dForm.value.cpf;
    this.associado.lotacao = this.dForm.value.lotacao;
    // this.associado.regiao = this.dForm.value.regiao;
    // this.associado.tipo = this.dForm.value.tipo;
    this.associado.status = this.dForm.value.status;

    this.updateSaveAssociado(this.associado, type);
  }

  // UPDATE SAVE ASSOCIADO
  updateSaveAssociado(associado, type) {
    this.loadingShow();
    this.associatedsService.updateSaveAssociado(associado).subscribe( response => {
      
      if(response) {
        this.associado = response;
        this.alertService.success({ title: 'Sucesso!', msg: 'Associado salvo com sucesso!' });
        this.typeAction(type);
      } else {
        this.alertService.warning({ title: 'Atenção!', msg: 'Não foi possível salvar o associado. Verifique os dados e tente novamente!' });
      }
      
      this.loadingHide();
    }, error => {
      console.log(error);
      this.loadingHide();
      this.alertService.sendMessage({ title: 'Erro!', msg: 'Não foi possível salvar o associado!' });
    });
  }

  // FIND BY ID ASSOCIADO
  findAssociadoById(id) {
    this.loadingShow();
    return this.associatedsService.findAssociadoById(id).subscribe( response => {
      this.associadoDB = response;
      this.dForm.patchValue({
        id: this.associadoDB.id,
        nome: this.associadoDB.nome,
        email: this.associadoDB.email,
        dataNascimento: this.associadoDB.dataNascimento,
        fone: this.associadoDB.fone,
        matricula: this.associadoDB.matricula,
        cpf: this.associadoDB.cpf,
        lotacao: this.associadoDB.lotacao,
        // regiao: this.associadoDB.regiao,
        // tipo: this.associadoDB.tipo,
        status: this.associadoDB.status
      });
      this.loadingHide();
    }, error => {
      console.log(error);
      this.loadingHide();
      this.alertService.sendMessage({ title: 'Erro!', msg: 'Não foi possível carregar o associado!' });
    });
  }

  // FORMATANDO A DATA PARA PERSISTIR
  formatData(d) {
    let dia = d.slice(0, 2);
    let mes = d.slice(2, 4);
    let ano = d.slice(4, 8);
    let dt = dia + '/' + mes +'/' + ano;
    console.log(dt);
    return dt;
  }
  
  // REDIRECIONAMENTO 
  private typeAction(type) {
    if (type === 'LISTAGEM') {
      this.router.navigate(['/associateds']);
    } else if (type === 'CADASTRO') {
      this.ngOnInit();
    }
  }

}
