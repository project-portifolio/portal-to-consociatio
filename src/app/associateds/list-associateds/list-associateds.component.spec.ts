import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAssociatedsComponent } from './list-associateds.component';

describe('ListAssociatedsComponent', () => {
  let component: ListAssociatedsComponent;
  let fixture: ComponentFixture<ListAssociatedsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListAssociatedsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAssociatedsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
