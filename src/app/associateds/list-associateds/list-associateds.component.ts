import { element } from 'protractor';
import { AssociatedsService } from './../associateds.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ExcelService } from '../../shared/services/excel/excel.service';
import * as XLSX from 'ts-xlsx';
import { NgxSpinnerService } from 'ngx-spinner';
import { AlertService } from '../../shared/componentes/alert/alert.service';
import { AuthGuard } from '../../shared';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-list-associateds',
  templateUrl: './list-associateds.component.html',
  styleUrls: ['./list-associateds.component.scss']
})
export class ListAssociatedsComponent implements OnInit {

  associados: any = [{ id: '', nome: '', cpf: '', email: '', fone: '', matricula: '', dataNascimento: '', lotacao: '', status: '', observacao: '', isCheked: '' }];
  listAssociados: any = [];
  list: any = [];
  arrayBuffer: any;
  file: File;
  nome: string;
  matricula: string;
  lotacao: string;
  textLabel: string = 'Expandir';
  textLabelNiver: string = 'Aniversariantes';
  hiddenFiltro = false;
  hiddenFiltroNiver = false;
  filter: any = {};
  page: any;
  p: any;
  user: any = {};
  hidden: boolean = false;
  listEmail: any = [];
  isTrue: boolean = false;
  closeResult: string;
  dataInicio: any;
  dataFim: any;

  constructor(private router: Router,
    private associatedsService: AssociatedsService,
    private excelService: ExcelService,
    private spinner: NgxSpinnerService,
    private alertService: AlertService,
    private authGuard: AuthGuard,
    private modalService: NgbModal) { 

      let verify = authGuard.canActivate();
      if(verify == true) {
        this.user = authGuard.getUserLogado();
        if(this.user.nivel == 5) {
          this.hidden = true;
        } else {
          this.hidden = false;
        }
      }

    }

  ngOnInit() {
    this.loadPage();
  }

  // LOADING SHOW
  loadingShow() {
    this.spinner.show();
  }

  // LOADING HIDE
  loadingHide() {
    this.spinner.hide();
  }

  loadPage() {
    if (!this.filter.page) {
      this.filter.page = 1;
    }
    if (!this.filter.pageSize) {
      this.filter.pageSize = 10;
    }
    this.getAllAssociados();
  }

  // exportando arquivo excel com lista de usuários
  exportAsXLSX():void {
    this.excelService.exportAsExcelFile(this.associados, 'lista_de_associados');
  }

  // escolhendo arquivo excel
  incomingfile(event) {
    this.file = event.target.files[0]; 
  }

  // cadastrar novo associado
  openRegister() {
    this.router.navigate(['associateds/register']);
  }

  // editar associado
  onEdit(a) {
    this.router.navigate(['associateds/details/' + a.id]);
  }

  public uploadData(event: any) : void { 
    // get data from file upload       
    let filesData = event.target.files;
    console.log(filesData[0]);
  }

  getAllAssociados() {
    this.loadingShow();
    this.associatedsService.getAll().subscribe( response => {
      this.associados = response;
      this.loadingHide();
      if(this.associados.length < 1) {
        this.alertService.warning({ title: 'Erro!', msg: 'Nenhum associado encontrado!' });
      }
    }, error => {
      console.log(error);
      this.loadingHide();
      this.alertService.sendMessage({ title: 'Erro!', msg: 'Não foi possível carregar a lista de associados!' });
    });
  }

  // fazendo upload de arquivo excel
  Upload() {
    this.loadingShow();
    let fileReader = new FileReader();
      fileReader.onload = (e) => {
        this.arrayBuffer = fileReader.result;
        var data = new Uint8Array(this.arrayBuffer);
        var arr = new Array();
        for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
        var bstr = arr.join("");
        var workbook = XLSX.read(bstr, {type:"binary"});
        var first_sheet_name = workbook.SheetNames[0];
        var worksheet = workbook.Sheets[first_sheet_name];
        console.log(XLSX.utils.sheet_to_json(worksheet,{raw:true}));
        this.listAssociados = XLSX.utils.sheet_to_json(worksheet,{raw:true});
        for(let i = 0; i < this.listAssociados.length; i++) {
          let d = this.listAssociados[i].dataNascimento;
          this.listAssociados[i].dataNascimento = d.replace(/\./g, '/');
          
          if(this.listAssociados[i].dataNascimento == '-') {
            console.log('Erro no campo data de nascimento do formulário de importação!');
            this.listAssociados[i].dataNascimento = 1;
            console.log(this.listAssociados[i].dataNascimento);
          } else if(!(this.listAssociados[i].dataNascimento == '-' && (this.listAssociados[i].nome == null || this.listAssociados[i].nome == undefined))) {
            this.list.push(this.listAssociados[i]);
          }
          
        }
        this.saveListAssociados(this.list);
      }
      if(!this.file) {
        this.loadingHide();
        this.alertService.warning({ title: 'Atenção!', msg: 'Nenhuma planilha selecionada!' });
        return;
      }
      fileReader.readAsArrayBuffer(this.file);
  }

  // salvando lista de associados
  saveListAssociados(listAssociados) {
    this.loadingShow();
    this.associatedsService.saveListAssociados(listAssociados).subscribe( response => {
      this.associados = response;
      this.alertService.success({ title: 'Sucesso!', msg: 'Planilha importada com sucesso!' });
      this.ngOnInit();
      this.loadingHide();
    }, error => {
      console.log(error);
      this.alertService.sendMessage({ title: 'Erro!', msg: 'Não foi possível importar a planilha de associados!' });
      this.loadingHide();
    });
  }

  // buscando o associado pelo nome
  getAssociadoByLikeNome(nome) {
    this.lotacao = null;
    this.matricula = null;
    if(nome == '') {
      this.ngOnInit();
      return;
    }
    this.loadingShow();
    this.associatedsService.getAssociadoByLikeNome(nome).subscribe( response => {
      this.associados = response;
      this.loadingHide();
      if(this.associados.length < 1) {
        this.alertService.warning({ title: 'Erro!', msg: 'Nenhum associado encontrado!' });
      }
    }, error => {
      console.log(error);
      this.loadingHide();
    });
  }

  // buscando o associado pela lotação
  getAssociadoByLotacao(lotacao) {
    this.nome = null;
    this.matricula = null;
    if(lotacao == '') {
      this.ngOnInit();
      return;
    }
    this.loadingShow();
    this.associatedsService.getAssociadoByLotacao(lotacao).subscribe( response => {
      this.associados = response;
      this.loadingHide();
      if(this.associados.length < 1) {
        this.alertService.warning({ title: 'Erro!', msg: 'Nenhum associado encontrado!' });
      }
    }, error => {
      console.log(error);
      this.loadingHide();
    });
  }

  // buscando o associado pela matrícula
  getAssociadoByLikeMatricula(matricula) {
    this.nome = null;
    this.lotacao = null;
    if(matricula == '') {
      this.ngOnInit();
      return;
    }
    this.loadingShow();
    this.associatedsService.filterByMatricula(matricula).subscribe( response => {
      this.associados = response;
      this.loadingHide();
      if(this.associados.length < 1) {
        this.alertService.warning({ title: 'Erro!', msg: 'Nenhum associado encontrado!' });
      }
    }, error => {
      console.log(error);
      this.loadingHide();
    });
  }

  // atualizando status
  updateStatus(associado) {
    console.log(associado);
    this.associatedsService.updateStatus(associado).subscribe( response => {
      console.log(response);
      console.log('Atualizando o associado!');
      this.alertService.success({ title: 'Sucesso!', msg: 'Status alterado com sucesso!' });
      this.getAllAssociados();
    }, error => {
      console.log(error);
      this.alertService.sendMessage({ title: 'Erro!', msg: 'Não foi possível alterar o status!' });
    });
  }

  // remover associado
  removeAssociado(associado) {
    this.loadingShow();
    this.associatedsService.removeAssociado(associado).subscribe( response => {
      console.log(response);
      this.alertService.success({ title: 'Sucesso!', msg: 'Associado excluído com sucesso!' });
      this.getAllAssociados();
      this.loadingHide();
    }, error => {
      console.log(error);
      this.loadingHide();
      this.alertService.sendMessage({ title: 'Erro!', msg: 'Não foi possível excluír associado!' });
    });
  }

  // EXPANDIR/OCULTAR FILTROS
  toggleFiltro() {
    if(this.hiddenFiltro) {
      this.hiddenFiltro = false;
      this.textLabel = 'Expandir';
    } else {
      this.hiddenFiltro = true;
      this.textLabel = 'Ocultar';
    }
  }

  // EXPANDIR/OCULTAR FILTROS
  toggleFiltroNiver() {
    if(this.hiddenFiltroNiver) {
      this.hiddenFiltroNiver = false;
      this.textLabelNiver = 'Aniversariantes';
    } else {
      this.hiddenFiltroNiver = true;
      this.textLabelNiver = 'Ocultar';
    }
  }

  // adicionar email no array
  adicionarEmail(email) {
    if(email == '-' || email == '' || email == null && email == undefined) {
      return;
    }
    let i = this.listEmail.indexOf(email);
      if(i == -1) {
        this.listEmail.push(email);
        console.log(this.listEmail.length + ', EMAIL SELECIONADO(S).');
      } else {
        this.listEmail.splice(i, 1);
        console.log(this.listEmail.length + ', EMAIL SELECIONADO(S).');
      }
  }

  isSelected() {
    if(this.isTrue) {
      this.isTrue = false;
      this.listEmail = [];
      console.log('NENHUM EMAIL SELECIONADO, ' + this.listEmail.length);
    } else {
      this.isTrue = true;
      this.associados.forEach(element => {
        element.isCheked = this.isTrue;
        this.adicionarEmail(element.email);
      });
    }
    
  }

  // ABRINDO MODAL DE CONFIRMAÇÃO DE REGISTRO
  openModalDelete(content, associado) {
    this.modalService.open(content, { centered: true }).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
        if(result === 'Confirm click') {
          console.log('Deletando registro!');
          this.removeAssociado(associado);
        } else if(result === 'Close click') {
          console.log('Cancelar operação!');
        }
    }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  // CAPTURANDO OPÇÕES DE FECHAMENTO DE MODAL DE EXCLUSÃO DE REGISTRO
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
        return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return 'by clicking on a backdrop';
    } else {
        return  `with: ${reason}`;
    }
  }

  // CAPTURANDO A DATA INICIAL PARA PESQUISA DE ANIVERSARIANTE 
  writeDataInicio(value) {
    this.dataInicio = value;
  }

  // CAPTURANDO A DATA FINAL PARA PESQUISA DE ANIVERSARIANTE
  writeDataFim(value) {
    this.dataFim = value;
  }

  // PESQUISANDO OS ANIVERSARIANTES
  getListAniversariantes() {
    this.loadingShow();
    if(!this.dataFim) {
      this.dataFim = this.dataInicio;
    }
    this.associatedsService.getListAniversariantes(this.dataInicio, this.dataFim).subscribe( response => {
      console.log(response);
      this.associados = response;
      this.loadingHide();
      if(this.associados.length < 1) {
        this.alertService.warning({ title: 'Erro!', msg: 'Nenhum associado encontrado!' });
      }
    }, error => {
      console.log(error);
    });
  }
  
}
