import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';

@Injectable()
export class LoginService {

  constructor(private http: HttpClient,
    private api: ApiService) { }
  
    // AUTENTICAÇÃO/LOGIN
    public login(user) {
      return this.http.post(this.api.getBaseUrl() + 'usuario/login', user, this.api.getOptions());
    }

}
