import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { LoginRoutingModule } from './login-routing.module';
import { LoginPageComponent } from './login-page/login-page.component';

import { LoginService } from './login.service';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
    imports: [
        CommonModule, 
        LoginRoutingModule,
        FormsModule,
        SharedModule
    ],
    declarations: [
        LoginPageComponent
    ],
    exports: [

    ],
    providers: [
        LoginService
    ]
})
export class LoginModule {}
