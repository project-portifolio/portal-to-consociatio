import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../../../router.animations';
import { LoginService } from '../login.service';
import { AuthGuard } from '../../../shared';
import { RouteExceptionsService } from '../../../shared/route-exceptions/route-exceptions.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { AlertService } from '../../../shared/componentes/alert/alert.service';

@Component({
    selector: 'app-login-page',
    templateUrl: './login-page.component.html',
    styleUrls: ['./login-page.component.scss'],
    animations: [routerTransition()]
})
export class LoginPageComponent implements OnInit {

    user: any = { email: '', senha: '' };
    isUser: any  = {};

    constructor(public router: Router,
    private loginService: LoginService,
    private authGuard: AuthGuard,
    private routeExceptionsService: RouteExceptionsService,
    private spinner: NgxSpinnerService,
    private alertService: AlertService) {
      authGuard.logout();
    }

    ngOnInit() {
    }

    // LOADING SHOW
    loadingShow() {
      this.spinner.show();
    }

    // LOADING HIDE
    loadingHide() {
      this.spinner.hide();
    }

    // AUTENTICAÇÃO - LOGIN
    onLoggedin() {
      this.loadingShow();
      this.loginService.login(this.user).subscribe(
        response => {
          this.isUser = response;
          if(this.isUser.status == true) {    
            this.savingUser(this.isUser);
          } else {
            this.alertService.info({ title: 'Atenção!', msg: 'Aprovação pendente!' });
          }
          this.loadingHide();
        }, error => {
          console.log(error.status);
          this.loadingHide();
          this.alertService.sendMessage({ title: 'Atenção!', msg: 'Verifique os dados de acesso e tente novamente!' });
        }
      );
    }

    // GUARDANDO USUÁRIO NO CACHE DO NAVEGADOR E LOGANDO
    savingUser(usuario) {
      this.authGuard.login(usuario);
    }

}
