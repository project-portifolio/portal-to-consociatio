import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PasswordRoutingModule } from './password-routing.module';
import { PasswordService } from './password.service';
import { InputEmailComponent } from './input-email/input-email.component';
import { InputCodigoComponent } from './input-codigo/input-codigo.component';
import { InputNewPasswordComponent } from './input-new-password/input-new-password.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    PasswordRoutingModule,
    FormsModule,
    SharedModule
  ],
  declarations: [
    InputEmailComponent,
    InputCodigoComponent,
    InputNewPasswordComponent
  ],
  providers: [
    PasswordService
  ]
})
export class PasswordModule { }
