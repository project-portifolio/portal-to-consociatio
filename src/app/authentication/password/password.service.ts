import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../../shared/services/api.service';
import { EmailValidator } from '@angular/forms';

@Injectable()
export class PasswordService {

  constructor(private http: HttpClient,
    private api: ApiService) { }

    email: string;

    // RETURNA E-MAIL DO USUÁRIO
    getEmail() {
      return this.email;
    }

    // GUARDA E-MAIL DO USUÁRIO
    setEmail(email) {
      console.log(email);
      this.email = email;
    }
    
    // VERIFICAR SE EXISTE E-MAIL NA BASE
    public verifica(user) {
      return this.http.post(this.api.getBaseUrl() + 'usuario/verifica', user, this.api.getOptions());
    }

    // CONFIRMA CÓDIGO
    public confirmRecovery(user) {
      return this.http.post(this.api.getBaseUrl() + 'usuario/confirmRecovery', user, this.api.getOptions());
    }

    // SETANDO NOVA SENHA
    public setNewPassword(user) {
      return this.http.post(this.api.getBaseUrl() + 'usuario/setNewPassword', user, this.api.getOptions());
    }

}
