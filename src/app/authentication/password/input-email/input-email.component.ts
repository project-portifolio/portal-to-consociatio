import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PasswordService } from '../password.service';
import { AlertService } from '../../../shared/componentes/alert/alert.service';

@Component({
  selector: 'app-input-email',
  templateUrl: './input-email.component.html',
  styleUrls: ['./input-email.component.scss']
})
export class InputEmailComponent implements OnInit {

  user: any = {};

  constructor(public router: Router,
    private passwordService: PasswordService,
    private alertService: AlertService) { }

  ngOnInit() {
  }

  // VERIFICANDO SE EXISTE E-MAIL NA BASE DE DADOS
  isExistEmail() {
    if(!this.user.email) {
      this.alertService.sendMessage({ title: 'Atenção!', msg: 'Preencha o campo e-mail e tente novamente!'});
      return;
    } 
    this.passwordService.verifica(this.user).subscribe( response => {
      if(response) {
        this.passwordService.setEmail(this.user.email);
        this.router.navigate(['/password/codigo']);
      } else {
        this.alertService.sendMessage({ title: 'Atenção!', msg: 'E-mail não encontrado, confirme e tente novamente!'});
      }
    }, error => {
      console.log(error);
    });
  }

}
