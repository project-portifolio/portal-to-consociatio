import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PasswordService } from '../password.service';
import { AlertService } from '../../../shared/componentes/alert/alert.service';

@Component({
  selector: 'app-input-new-password',
  templateUrl: './input-new-password.component.html',
  styleUrls: ['./input-new-password.component.scss']
})
export class InputNewPasswordComponent implements OnInit {

  user: any = {};
  confirPassword: string;

  constructor(public router: Router,
    private passwordService: PasswordService,
    private alertService: AlertService) { }

  ngOnInit() {
  }

  // SETANDO NOVA SENHA
  setNewPassword() {
    if(!this.user.senha || !this.confirPassword) {
      this.alertService.sendMessage({ title: 'Atenção!', msg: 'Preencha os campos e tente novamente!' });
      return;
    }
    if(this.user.senha === this.confirPassword && this.user.senha != null && this.confirPassword != null) {
      this.user.email = this.passwordService.getEmail();
      this.passwordService.setNewPassword(this.user).subscribe( response => {
        this.passwordService.setEmail('');
        this.router.navigate(['/login']);
      }, error => {
        console.log(error);
      });
    } else {
      this.alertService.sendMessage({ title: 'Atenção!', msg: 'As senhas não conferem!' });
    }
  }

}
