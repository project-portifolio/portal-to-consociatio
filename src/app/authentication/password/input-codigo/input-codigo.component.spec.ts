import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputCodigoComponent } from './input-codigo.component';

describe('InputCodigoComponent', () => {
  let component: InputCodigoComponent;
  let fixture: ComponentFixture<InputCodigoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputCodigoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputCodigoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
