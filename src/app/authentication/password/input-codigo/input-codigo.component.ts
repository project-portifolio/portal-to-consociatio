import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PasswordService } from '../password.service';
import { AlertService } from '../../../shared/componentes/alert/alert.service';

@Component({
  selector: 'app-input-codigo',
  templateUrl: './input-codigo.component.html',
  styleUrls: ['./input-codigo.component.scss']
})
export class InputCodigoComponent implements OnInit {

  user: any = {};

  constructor(public router: Router,
    private passwordService: PasswordService,
    private alertService: AlertService) { }

  ngOnInit() {
  }

  // CONFIRMAÇÃO DE CÓDIGO
  confirmRecovery() {
    if(!this.user.codigo) {
      this.alertService.sendMessage({ title: 'Atenção!', msg: 'Prencha o campo e tente novamente!' });
      return;
    }
    this.passwordService.confirmRecovery(this.user).subscribe( response => {
      console.log(response);
      if(response) {
        this.router.navigate(['/password/senha']);
      } else {
        this.alertService.sendMessage({ title: 'Atenção!', msg: 'Verifique código inválido!' });
      }
    }, error => {
      console.log(error);
    });
  }

}
