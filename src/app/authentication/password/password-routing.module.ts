import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InputEmailComponent } from './input-email/input-email.component';
import { InputCodigoComponent } from './input-codigo/input-codigo.component';
import { InputNewPasswordComponent } from './input-new-password/input-new-password.component';

const routes: Routes = [
  { path: 'email', component: InputEmailComponent },
  { path: 'codigo', component: InputCodigoComponent },
  { path: 'senha', component: InputNewPasswordComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PasswordRoutingModule { }
