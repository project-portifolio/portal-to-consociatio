import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { RegisterUserComponent } from './register-user/register-user.component';
import { RegisterRoutingModule } from './register-routing.module';
import { RegisterService } from './register.service';
import { SharedModule } from '../../shared/shared.module';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  imports: [
    CommonModule,
    RegisterRoutingModule,
    FormsModule,
    SharedModule,
    NgxMaskModule.forRoot()
  ],
  declarations: [
    RegisterUserComponent
  ],
  exports: [
    RegisterUserComponent
  ],
  providers: [
    RegisterService
  ]
})
export class RegisterModule { }
