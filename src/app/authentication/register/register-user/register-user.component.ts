import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../../../router.animations';
import { RegisterService } from '../register.service';
import { RouteExceptionsService } from '../../../shared/route-exceptions/route-exceptions.service';
import { AlertService } from '../../../shared/componentes/alert/alert.service';

@Component({
    selector: 'app-register-user',
    templateUrl: './register-user.component.html',
    styleUrls: ['./register-user.component.scss'],
    animations: [routerTransition()]
})
export class RegisterUserComponent implements OnInit {

    user: any = { nome: '', email: '', senha: '', fone: '' };
    isUser: any  = {};
    confirPassword: string = '';

    constructor(public router: Router,
        private registerService: RegisterService,
        private routeExceptionsService: RouteExceptionsService,
        private alertService: AlertService) {}

    ngOnInit() {}

    // CADASTRAR USUÁRIO
    saveUser() {
        if(this.user.senha == this.confirPassword && this.user.nome != "" && this.user.email != "" && this.user.fone != "") {
            this.registerService.saveUser(this.user).subscribe( response => {
                this.isUser = response;
                
                if(this.isUser == true) {
                    this.alertService.warning({ title: 'Atenção!', msg: 'Email já cadastrado!' });
                } else {
                    this.user = {};
                    this.confirPassword = '';
                    this.alertService.success({ title: 'Sucesso!', msg: 'Usuário cadastrado com sucesso!' });
                }
                
            }, error => {
                this.alertService.sendMessage({ title: 'Erro!', msg: 'Verifique sua conexão com a internet e tente novamente!' });
            });
        } else {
            this.user.senha = '';
            this.confirPassword = '';
            this.alertService.warning({ title: 'Atenção!', msg: 'Preencha todos os dados do formulário!' });
      }

    }
}
