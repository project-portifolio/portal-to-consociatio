import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../../shared/services/api.service';

@Injectable()
export class RegisterService {

  constructor(private http: HttpClient,
    private api: ApiService) { }

  // CADASTRANDO USUÁRIO
  public saveUser(user) {
    return this.http.post(this.api.getBaseUrl() + 'usuario/cadastrarUsuario', user, this.api.getOptions());
  }

}
