import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RolesRoutingModule } from './roles-routing.module';
import { ListRolesComponent } from './list-roles/list-roles.component';
import { ProfileRoleComponent } from './profile-role/profile-role.component';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { RolesService } from './roles.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    RolesRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule.forRoot()
  ],
  declarations: [
    ListRolesComponent, 
    ProfileRoleComponent
  ],
  exports: [
    ListRolesComponent, 
    ProfileRoleComponent,
    NgbModule
  ],
  providers: [
    RolesService
  ]
})
export class RolesModule { }
