import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListRolesComponent } from './list-roles/list-roles.component';
import { ProfileRoleComponent } from './profile-role/profile-role.component';

const routes: Routes = [
    { path: '', component: ListRolesComponent },
    { path: '', component: ProfileRoleComponent },
    { path: ':id', component: ProfileRoleComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RolesRoutingModule { }
