import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RolesService } from '../roles.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-list-roles',
  templateUrl: './list-roles.component.html',
  styleUrls: ['./list-roles.component.scss']
})
export class ListRolesComponent implements OnInit {

  permissoes: any = [{id: '', titulo:'', autorizacao: '', descricao: ''}];

  constructor(private router: Router,
    private rolesService: RolesService,
    private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.getAllPermissoes();
  }

  // LOADING SHOW
  loadingShow() {
    this.spinner.show();
  }

  // LOADING HIDE
  loadingHide() {
    this.spinner.hide();
  }

  // IR PARA O FORMULÁRIO DE PERMISSÃO
  openRegister() {
    this.router.navigate(['roles/register']);
  }

  // EDITAR PERMISSÃO
  onEdit(p) {
    this.router.navigate(['roles/details/' + p.id]);
  }

  // REMOVER PERMISSÃO
  removePermissao(p) {
    this.rolesService.removePermissao(p).subscribe( response => {
      this.getAllPermissoes();
    }, error => {
      console.log(error);
      this.loadingHide();
    });
  }

  // BUSCANDO TODAS AS PERMISSÕES
  getAllPermissoes() {
    this.loadingShow();
    this.rolesService.getAllPermissoes().subscribe( response => {
      this.permissoes = response;
      this.loadingHide();
    }, error => {
      console.log(error);
      this.loadingHide();
    });
  }

}
