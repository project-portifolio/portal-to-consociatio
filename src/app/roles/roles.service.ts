import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './../shared/services/api.service';

@Injectable()
export class RolesService {

  constructor(private http: HttpClient, 
    private api: ApiService) { }

    // ATUALIZAR/SALVAR PERMISSÃO
    public updateSaveAccess(permissao) {
      return this.http.post(this.api.getBaseUrl() + 'permissao/updateSavePermissao', permissao, this.api.getOptions());
    }

    // BUSCANDO TODAS AS PERMISSÕES
    public getAllPermissoes() {
      return this.http.get(this.api.getBaseUrl() + 'permissao/getAllPermissoes', this.api.getOptions());
    }
    
    // BUSCANDO A PERMISSÃO PELO ID
    public getPermissaoById(id) {
      return this.http.get(this.api.getBaseUrl() + 'permissao/getPermissaoById/' + id, this.api.getOptions());
    }

    //  REMOVENDO PERMISSÃO
    public removePermissao(p){
      return this.http.delete(this.api.getBaseUrl() + 'permissao/removePermissao/' + p.id, this.api.getOptions());
    }

}
