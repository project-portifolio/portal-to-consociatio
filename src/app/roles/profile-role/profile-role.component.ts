import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RolesService } from '../roles.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-profile-role',
  templateUrl: './profile-role.component.html',
  styleUrls: ['./profile-role.component.scss']
})
export class ProfileRoleComponent implements OnInit {

  id: number;
  title: string;
  labelSuccess: string;
  dForm: FormGroup;
  permissao: any = {id: '', autorizacao: '', descricao: '', titulo: ''};

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private rolesService: RolesService,
    private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.criaForm();
    // RECUPERANDO O ID PASSADO COMO PARÂMETRO NA URL
    this.activatedRoute.params.subscribe( params => {
      if(!(params && params.id && params.id != 'register')) {
        this.title = 'Nova permissão';
        this.labelSuccess = 'Cadastrar';
        return [];
      } else {
        console.log(params.id);
        this.id = +params['id'];
        this.title = 'Editar permissão';
        this.labelSuccess = 'Salvar';
        this.getPermissaoById(this.id);
      }
    });
  }

  // LOADING SHOW
  loadingShow() {
    this.spinner.show();
  }

  // LOADING HIDE
  loadingHide() {
    this.spinner.hide();
  }

  ngOnSubmit() {
    console.log(this.dForm.value);
    this.permissao.id = this.dForm.value.id;
    this.permissao.autorizacao = this.dForm.value.autorizacao;
    this.permissao.descricao = this.dForm.value.descricao;
    this.permissao.titulo = this.dForm.value.titulo;
    console.log(this.permissao);
    this.updateSaveAccess(this.permissao);
  }

  // CRIANDO FORMULÁRIO
  criaForm() {
    this.dForm = this.fb.group({
      id: [null],
      autorizacao: [null, Validators.required],
      descricao: [null, Validators.required],
      titulo: [null, Validators.required]
    });
  }

  // ATUALIZAR/SALVAR PERMISSÃO
  updateSaveAccess(permissao) {
    this.loadingShow();
    this.rolesService.updateSaveAccess(permissao).subscribe( response => {
      this.permissao = response;
      this.router.navigate(['roles']);
      this.loadingHide();
    }, error => {
      console.log(error);
      this.loadingHide();
    });
  }

  getPermissaoById(id) {
    this.loadingShow();
    this.rolesService.getPermissaoById(id).subscribe(response => {
      this.permissao = response;
      this.dForm.patchValue({
        id: this.permissao.id,
        autorizacao: this.permissao.autorizacao,
        descricao: this.permissao.descricao,
        titulo: this.permissao.titulo
      });
      this.loadingHide();
    }, error => {
      console.log(error);
      this.loadingHide();
    });
  }

}
