import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccessDeniedRoutingModule } from './access-denied-routing.module';
import { DeniedComponent } from './denied/denied.component';

@NgModule({
  imports: [
    CommonModule,
    AccessDeniedRoutingModule
  ],
  declarations: [DeniedComponent]
})
export class AccessDeniedModule { }
