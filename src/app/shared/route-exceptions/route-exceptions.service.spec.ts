import { TestBed, inject } from '@angular/core/testing';

import { RouteExceptionsService } from './route-exceptions.service';

describe('RouteExceptionsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RouteExceptionsService]
    });
  });

  it('should be created', inject([RouteExceptionsService], (service: RouteExceptionsService) => {
    expect(service).toBeTruthy();
  }));
});
