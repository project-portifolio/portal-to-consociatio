import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RouteExceptionsService {

  constructor(private router: Router) { }

  redirectRoute(status: any) {

    if(status === false) {
      status = 403;
    }

    switch(status) {
      case 0: {
        this.router.navigate(['/not-found']);
        break;
      }
      case 400: {
        this.router.navigate(['/error']);
        break;
      }
      case 403: {
        this.router.navigate(['/access-denied']);
        break;
      }
      default: { 
        this.router.navigate(['/login']);
        break; 
     } 

    }
  }

}
