import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageHeaderComponent } from './componentes/page-header/page-header.component';
import { RouterModule } from '@angular/router';
import { ExcelService } from './services/excel/excel.service';
import { NgxSpinnerModule } from 'ngx-spinner';
import { SpinnerComponent } from './componentes/spinner/spinner.component';
import { PaginationComponent } from './componentes/pagination/pagination.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { AlertService } from './componentes/alert/alert.service';
import { AlertComponent } from './componentes/alert/alert.component';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NgxSpinnerModule,
    NgxMaskModule.forRoot()
  ],
  declarations: [
    PageHeaderComponent,
    SpinnerComponent,
    PaginationComponent,
    AlertComponent
  ], 
  exports: [
    PageHeaderComponent,
    SpinnerComponent,
    NgxSpinnerModule,
    NgxPaginationModule,
    PaginationComponent,
    AlertComponent
  ],
  providers: [
    ExcelService,
    AlertService
  ]
})
export class SharedModule { }
