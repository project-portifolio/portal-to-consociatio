import { Component, Input, ViewChild, OnDestroy } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbModalRef  } from '@ng-bootstrap/ng-bootstrap';
import { OnInit } from '@angular/core';
import { SendEmailService } from './send-email.service';
import { AlertService } from '../alert/alert.service';

@Component({
  selector: 'app-modal-email',
  templateUrl: './modal-email.component.html',
  styleUrls: ['./modal-email.component.scss']
})
export class ModalEmailComponent{

  @Input() listEmails: any = [];
  body: string = '';
  titulo: string = '';
  fileName: string = '';
  fileBase64 = null;
  
  closeResult: string;
  IsmodelShow: boolean;
  sendMail: any = {titulo: '', msg: '', listEmails: '', nameFile: '', urlFile: ''};
  
    constructor(private modalService: NgbModal,
        private sendEmailService: SendEmailService,
        private alertService: AlertService) {}

    // CAPTURANDO O INPUT DO TÍTULO DA MENSAGEM 
    writeTextTitulo(value) {
        this.titulo = value;
    }

    // CAPTURANDO A MENSAGEM DO TEXTAREA DO MODAL
    writeTextMsg(value) {
      this.body = value;
    }

    // ABRINDO O MODAL
    open(content) {
        this.openModal(content);
    }

    // FECHANDO O MODAL
    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    // ABRINDO O MODAL DE ENVIO DE EMAILS
    openModal(content) {
        this.modalService.open(content, { size: 'lg', centered: true }).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            if(reason == 'Cross click') {
                this.body = '';
                this.titulo = '';
                return;
            }
            if(reason == 'email' && this.listEmails == '') {
                this.alertService.warning({ title: 'Atenção!', msg: 'Nenhum associado selecionado!' });
                return;
            }
            if(reason == 'email' && this.titulo == '') {
                this.alertService.warning({ title: 'Atenção!', msg: 'Título do email vázio!' });
                return;
            }
            if(reason == 'email' && this.body == '') {
                this.alertService.warning({ title: 'Atenção!', msg: 'Corpo do email vázio!' });
                return;
            }
            this.sendMail.nameFile = this.fileName;
            this.sendMail.titulo = this.titulo;
            this.sendMail.msg = this.body;
            this.sendMail.listEmails = this.listEmails;
            this.sendMail.urlFile = this.fileBase64;
            if(this.sendMail.listEmails != '' && this.sendMail.msg != '' && this.sendMail.titulo != '') {
                console.log('Enviando o email');
                this.sendEmail(this.sendMail);
            } else {
                this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            }
        })
    }

    // ENVIANDO O(s) EMAIL(s)
    sendEmail(emails) {
        this.sendEmailService.sendEmail(emails).subscribe( (response: any) => {
            if(response.success == 1) {
                this.alertService.success({ title: 'Sucesso!', msg: 'Email(s) enviado(s) com sucesso!' });
            } else {
                this.alertService.sendMessage({ title: 'Erro!', msg: 'Não foi possível enviar o(s) email(s)!' });
            }
            this.titulo = '';
            this.body = '';
        }, error => {
            console.log(error);
            this.alertService.sendMessage({ title: 'Falha!', msg: 'Não foi possível enviar o(s) email(s)!' });
        });
    }

    // escolhendo arquivo anexo
    inputFileChange(event) {
        let file: FileList = event.target.files;
        this.fileName = file[0].name;
       
        let reader = new FileReader();
        let [files] = event.target.files;

        reader.readAsDataURL(files);
        reader.onload = () => {
            this.fileBase64 = reader.result;
            console.log(this.fileBase64);
        }
    }

}
