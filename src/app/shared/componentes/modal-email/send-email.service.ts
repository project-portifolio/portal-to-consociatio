import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../../services/api.service';
import { Observable } from 'rxjs';

@Injectable()
export class SendEmailService {
  httpClient: any;
  handleError: any;

  constructor(private http: HttpClient,
    private api: ApiService) { }

  // ENVIANDO EMAIL PARA O ASSOCIADO
  public sendEmail(emails) {
    return this.http.post(this.api.getBaseUrl() + 'sendEmail/emailAnexo', emails, this.api.getOptions());
  }

}
