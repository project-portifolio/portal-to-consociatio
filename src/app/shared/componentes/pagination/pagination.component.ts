import { Component, OnInit, Input, HostListener, OnChanges, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit, OnChanges {

  @Input()
  page;
  pages = [];

  @Output()
  clickPageButton: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {

  }

  ngOnChanges(change) {
    if (change.page && change.page.currentValue) {
      let totalRecords = change.page.currentValue.totalRecords;
      this.pages = this.createPages(totalRecords);
    }
  }

  createPages(totalRecords) {
    let pages = [];
    let totalPages = totalRecords / 10;
    for (let i = 0; i < totalPages; i++) {
      pages.push(i);
    }

    return pages;
  }

  onClickPageButton(p) {
    this.clickPageButton.emit(p);
  }

  onClickLeftButton(p) {
    this.clickPageButton.emit(p > 0 ? p - 1 : p);
  }

  onClickRightButton(p) {
    this.clickPageButton.emit(p < (this.pages.length - 1) ? p + 1 : p);
  }

  onClickFirstButton(p) {
    this.clickPageButton.emit(this.pages[0]);
  }

  onClickLastButton(p) {
    this.clickPageButton.emit(this.pages[this.pages.length - 1]);
  }

}
