import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { DashboardService } from './dashboard.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthGuard } from '../../shared';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    animations: [routerTransition()]
})
export class DashboardComponent implements OnInit {
    
    user: any = {};
   
    numberUsers: any = 0;
    numberAssociateds: any = 0;
    numberAssociatedsA: any = 0;
    numberAssociatedsB: any = 0;
    numberAssociatedsActives: any = 0;
    numberAssociatedsInactives: any = 0;

    constructor(private dashboardService: DashboardService,
        private spinner: NgxSpinnerService,
        private authGuard: AuthGuard) {
            this.user = authGuard.getUserLogado();
        }

    ngOnInit() {
        this.countAllUsers();
        this.countAllAssociadosInativos();
        this.countAllAssociados();
        this.countAllAssociadosAtivos();
    }

    // LOADING SHOW
    loadingShow() {
        this.spinner.show();
    }

    // LOADING HIDE
    loadingHide() {
        this.spinner.hide();
    }

    // CONTANDO OS USUÁRIOS
    countAllUsers() {
        this.loadingShow();
        this.dashboardService.getCountAllUsers().subscribe(response => {
            this.numberUsers = response;
            if(this.user.nivel != 5) {
                --this.numberUsers;
            } else {
                this.numberUsers = response;
            }
            this.loadingHide();
        }, error => {
            console.log(error);
            this.loadingHide();
        });
    }

     // CONTANDO OS ASSOCIADOS INATIVOS
     countAllAssociadosAtivos() {
        this.loadingShow();
        this.dashboardService.countAllAssociadosAtivos().subscribe(response => {
            this.numberAssociatedsActives = response;
            this.loadingHide();
        }, error => {
            console.log(error);
            this.loadingHide();
        });
    }

    // CONTANDO OS ASSOCIADOS INATIVOS
    countAllAssociadosInativos() {
        this.loadingShow();
        this.dashboardService.countAllAssociadosInativos().subscribe(response => {
            this.numberAssociatedsInactives = response;
            this.loadingHide();
        }, error => {
            console.log(error);
            this.loadingHide();
        });
    }

     // CONTANDO TODOS OS ASSOCIADOS 
     countAllAssociados() {
        this.loadingShow();
        this.dashboardService.countAllAssociados().subscribe(response => {
            this.numberAssociateds = response;
            this.loadingHide();
        }, error => {
            console.log(error);
            this.loadingHide();
        });
    }

}
