import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbCarouselModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { CardDashboardComponent } from './components/card-dashboard/card-dashboard.component';

import { DashboardService } from './dashboard.service';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        NgbCarouselModule.forRoot(),
        NgbAlertModule.forRoot(),
        DashboardRoutingModule,
        SharedModule
    ],
    declarations: [
        DashboardComponent,
        CardDashboardComponent
    ],
    providers: [
        DashboardService
    ]
})
export class DashboardModule {}
