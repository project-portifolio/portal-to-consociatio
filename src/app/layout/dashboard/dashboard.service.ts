import { Injectable } from '@angular/core';
import { ApiService } from './../../shared/services/api.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private api: ApiService,
    private http: HttpClient) { }

    getCountAllUsers() {
      return this.http.get(this.api.getBaseUrl() + 'usuario/countAllUsers', this.api.getOptions());
    }

    countAllAssociadosAtivos() {
      return this.http.get(this.api.getBaseUrl() + 'associados/countAllAssociadosAtivos', this.api.getOptions());
    }

    countAllAssociadosInativos() {
      return this.http.get(this.api.getBaseUrl() + 'associados/countAllAssociadosInativos', this.api.getOptions());
    }

    countAllAssociados() {
      return this.http.get(this.api.getBaseUrl() + 'associados/countAllAssociados', this.api.getOptions());
    }

}
