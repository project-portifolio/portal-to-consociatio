import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AuthGuard } from '../../../shared';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    pushRightClass: string = 'push-right';

    user: any = {};
    title: string = 'APROMUC';
    pathLogo: string = 'assets/images/logo-associacao.jpeg';

    constructor(private translate: TranslateService, 
        public router: Router,
        private authGuard: AuthGuard) {

        let verify = authGuard.canActivate();
        if(verify == true) {
            this.user = authGuard.getUserLogado();
        }

        this.translate.addLangs(['pt', 'en', 'fr', 'es', 'it', 'de']);
        this.translate.setDefaultLang('pt');
        const browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/pt|en|fr|es|it|de/) ? browserLang : 'pt');

        this.router.events.subscribe(val => {
            if (
                val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()
            ) {
                this.toggleSidebar();
            }
        });
    }

    ngOnInit() {}

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    // OCULTAR PARCIALMENTE O MENU
    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    // MOVE O SIDE-MENU PARA A ESQUERDA OU À DIREITA
    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    // LOGOUT - SAIR
    logout() {
        this.authGuard.logout();
    }

    // OPÇÕES DE IDIOMA
    changeLang(language: string) {
        this.translate.use(language);
    }

    goPerfil() {
        this.router.navigate(['users/details/' + this.user.id]);
    }
}
