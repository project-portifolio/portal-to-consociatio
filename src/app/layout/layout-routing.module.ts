import { ProfileUserComponent } from './../user/profile-user/profile-user.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'prefix' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'associateds', loadChildren: '../associateds/associateds.module#AssociatedsModule' },
            { path: 'associateds/', loadChildren: '../associateds/associateds.module#AssociatedsModule' },
            { path: 'associateds/:id', loadChildren: '../associateds/associateds.module#AssociatedsModule' },
            { path: 'users', loadChildren: '../user/user.module#UserModule' },
            { path: 'users/', loadChildren: '../user/user.module#UserModule' },
            { path: 'users/:id', loadChildren: '../user/user.module#UserModule' },
            { path: 'roles', loadChildren: '../roles/roles.module#RolesModule' },
            { path: 'roles/', loadChildren: '../roles/roles.module#RolesModule' },
            { path: 'roles/:id', loadChildren: '../roles/roles.module#RolesModule' },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
