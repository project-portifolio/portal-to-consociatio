import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared';

const routes: Routes = [
    { path: '', loadChildren: './layout/layout.module#LayoutModule', canActivate: [AuthGuard] },
    { path: 'login', loadChildren: './authentication/login/login.module#LoginModule' },
    { path: 'register', loadChildren: './authentication/register/register.module#RegisterModule' },
    { path: 'error', loadChildren: './exceptions/server-error/server-error.module#ServerErrorModule' },
    { path: 'access-denied', loadChildren: './exceptions/access-denied/access-denied.module#AccessDeniedModule' },
    { path: 'not-found', loadChildren: './exceptions/not-found/not-found.module#NotFoundModule' },
    { path: 'password', loadChildren: './authentication/password/password.module#PasswordModule' },
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    { path: '**', redirectTo: 'login' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {useHash: true})],
    exports: [RouterModule]
})
export class AppRoutingModule {}
